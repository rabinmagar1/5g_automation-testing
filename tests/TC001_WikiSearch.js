module.exports = {
  '@tags': ['smoke', 'integration'],
    before : function(browser) {
      //Declaring Global Timeout
      browser.globals.waitForConditionTimeout = 7000;
    },

    'Wikipedia Search' : function (browser) {
    browser
      .url('https://www.wikipedia.org/')
      .assert.title('Wikipedia')
      .setValue('#searchInput', 'Facebook')
      .click('button[type="submit"]')
      .assert.containsText('h1#firstHeading', 'Facebook')
      .assert.title('Facebook - Wikipedia')
      
  },

  after : function(browser) {
    browser.end();
  }

}